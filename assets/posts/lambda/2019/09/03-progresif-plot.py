import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

# pajak penghasilan = tarif progresif pasal 17
[t1, t2, t3] = [x * 10**6 for x in [50, 250, 500]]
def pph(pkp):
  if pkp <= t1:
    return pkp * 0.05
  elif pkp <= t2:
    return (pkp - t1) * 0.15 + pph(t1)
  elif pkp <= t3:
    return (pkp - t2) * 0.25 + pph(t2)
  else:
    return (pkp - t3) * 0.30 + pph(t3)

def currency(x, pos):
    """The two args are the value and tick position"""
    if x >= 1e6:
        s = 'Rp. {:1,.0f} juta'.format(x*1e-6)
    else:
        s = 'Rp. {:1,.0f} ribu'.format(x*1e-3)
    return s

formatter = FuncFormatter(currency)

xs    = np.arange(0, 6 * 10**8, 10**7)
xdots = np.arange(0, 6 * 10**8, 5*10**7)

ys    = [pph(x) for x in xs]
ydots = [pph(xdot) for xdot in xdots]

plt.rcParams.update({'figure.autolayout': True})

fig, ax = plt.subplots()

ax.plot(xs, ys, 'k', xdots, ydots, 'bo')
ax.axis([0, 6 * 10**8, 0, 125 * 10**6])
plt.xticks(np.arange(0, 6 * 10**8 + 1 ,      10**8))
plt.yticks(np.arange(0, 125 * 10**6 + 1, 2.5 * 10**7))

ax.axvline(x= 50  * 10**6, color='c')
ax.axhline(y= 2.5 * 10**6, color='c')

ax.axvline(x=250  * 10**6, color='m')
ax.axhline(y=32.5 * 10**6, color='m')

ax.axvline(x=500  * 10**6, color='y')
ax.axhline(y= 95  * 10**6, color='y')

ax.grid()

labels = ax.get_xticklabels()
plt.setp(labels,
         rotation=45,
         horizontalalignment='right')
ax.set(title='Tarif Progresif PPh 21',
       xlabel='PKP', ylabel='PPh')
ax.xaxis.label.set_size(16)
ax.yaxis.label.set_size(16)
ax.title.set_size(20)

ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)

plt.show()
